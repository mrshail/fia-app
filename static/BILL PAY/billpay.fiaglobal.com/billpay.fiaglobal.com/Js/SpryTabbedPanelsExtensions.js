// SpryTabbedPanelsExtensions.js - version 0.2 - Spry Pre-Release 1.6
//
// Copyright (c) 2006. Adobe Systems Incorporated.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   * Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//   * Neither the name of Adobe Systems Incorporated nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Override the default versions of getTabGroup and getContentPanelGroup
// to add the ability to place the tab buttons at the top or bottom of
// the widget. To specify the tab buttons at the bottom, the developer
// must pass a tabsAtBottom:true constructor option.

Spry.Widget.TabbedPanels.prototype.getTabGroup = function()
{
	if (this.element)
	{
		var children = this.getElementChildren(this.element);
		if (children.length)
			return children[this.tabsAtBottom ? 1 : 0];
	}
	return null;
};

Spry.Widget.TabbedPanels.prototype.getContentPanelGroup = function()
{
	if (this.element)
	{
		var children = this.getElementChildren(this.element);
		if (children.length)
			return children[this.tabsAtBottom ? 0 : 1];
	}
	return null;
};

// Override the default version of onTabClick so that it calls
// stop(), just in case the tabbed panels is cyclying through
// all of its tabs.

Spry.Widget.TabbedPanels.prototype._onTabClick = Spry.Widget.TabbedPanels.prototype.onTabClick;
Spry.Widget.TabbedPanels.prototype.onTabClick = function(e, tab)
{
	this.stop();
	return this._onTabClick(e, tab);
};

// Add the ability to step through the panels at a given interval.

Spry.Widget.TabbedPanels.prototype.showPreviousPanel = function()
{
	var curIndex = this.getCurrentTabIndex();
	this.showPanel(((curIndex < 1) ? this.getTabbedPanelCount() : curIndex) - 1);
};

Spry.Widget.TabbedPanels.prototype.showNextPanel = function()
{
	this.showPanel((this.getCurrentTabIndex()+1) % this.getTabbedPanelCount());
};

Spry.Widget.TabbedPanels.prototype.showFirstPanel = function()
{
	this.showPanel(0);
};

Spry.Widget.TabbedPanels.prototype.showLastPanel = function()
{
	var count = this.getTabbedPanelCount();
	this.showPanel(count > 0 ? count - 1 : 0);
};


Spry.Widget.TabbedPanels.prototype.start = function()
{
	this.stop();
	var self = this;
	if (!this.interval)
		this.interval = 2000;
	this.timerID = setTimeout(function() { self.fadeInNextPanel(); }, this.interval);
};

Spry.Widget.TabbedPanels.prototype.stop = function()
{
	if (this.timerID)
		clearTimeout(this.timerID);
	this.timerID = 0;
};

Spry.Widget.TabbedPanels.prototype.fadeInNextPanel = function()
{
	if (!Spry.Effect || !Spry.Effect.DoFade)
	{
		// The fade effect isn't available, so just
		// show the next panel.

		this.showNextPanel();
		if (this.timerID)
			this.start();
		return;
	}

	var curIndex = this.getCurrentTabIndex();
	var panels = this.getContentPanels();
	var currentPanel = panels[ curIndex ];
	var nextPanel = panels[ (curIndex + 1) % this.getTabbedPanelCount() ];
	var self = this;

	Spry.Effect.DoFade(currentPanel, {duration: 1000, from: 100, to: 0, toggle: false, finish: function()
	{
		nextPanel.style.opacity = '0';
		nextPanel.style.filter = 'alpha(opacity=0)';

		self.showPanel(nextPanel);

		currentPanel.style.opacity = '';
		currentPanel.style.filter = '';
		Spry.Effect.DoFade(nextPanel, {duration: 1000, from: 0, to: 100, toggle: false, finish: function()
		{
			if (self.timerID)
				self.start();
		}});
	}});
};
