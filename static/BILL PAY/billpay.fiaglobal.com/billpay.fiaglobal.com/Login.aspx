
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1"><title>
	FiaGlobal - Instant Bill Payment
</title><link rel="stylesheet" type="text/css" href="css/brand.css" />
    <script language="JavaScript" type="text/javascript" src="Js/SpryEffects.js"></script>
    <script language="JavaScript" type="text/javascript" src="Js/SpryTabbedPanels.js"></script>
    <script language="JavaScript" type="text/javascript" src="Js/SpryTabbedPanelsExtensions.js"></script>
<style type="text/css">
        .hcenter
{
    background-color: #FFFFFF;
    border-width: 5px;
    border-style: solid;
    border-color: purple;
    padding-top: 10px;
    padding-left: 10px;
    width: 330px;
    height: 140px;
}
    </style>
    <script type="text/javascript">
        function toUpper(txt) {
            document.getElementById(txt).value = document.getElementById(txt).value.toUpperCase();
            return true;
        }

        function EncryptDecrypt(txt) {
            var asc;
            var hex = '';
            var str = document.getElementById(txt).value;
            var n = str.length;
            var r1 = Math.floor(Math.random() * 10)
            hex += r1;
            for (var i = 0; i < n; i++) {
                asc = (str.charCodeAt(i)) - ((i + 1) ^ (i + 2));
                asc = asc + r1
                hex += String.fromCharCode(asc);
            }
            document.getElementById(txt).value = hex;
        }

        function HashLogin() {
            EncryptDecrypt('txtPassword');
        }

        function HashChgPwd() {
            var expr = new RegExp(/(?=^.{8,20}$)(?=(?:.*?\d){1})(?=.*[a-z])(?=(?:.*?[!@#$%*()_+^&}{:;?.]){1})(?!.*\s)[0-9a-zA-Z!@#$%*()_+^&]*$/);
            var NewPwd = document.getElementById('txtNewPwd').value
            var CnfPwd = document.getElementById('txtConfirmPwd').value

            if (NewPwd != CnfPwd) {
                alert('{*} New Pwd and Confirm Pwd should be same.')
                document.getElementById('txtNewPwd').value = ""
                document.getElementById('txtNewPwd').focus();
                document.getElementById('txtConfirmPwd').value = ""
                return false;
            }

            if (NewPwd.length >= 8 && expr.test(NewPwd)) {
                EncryptDecrypt('txtNewPwd');
                document.getElementById('txtConfirmPwd').value = "";
                // EncryptDecrypt('txtConfirmPwd');
            }
            else {
                alert('{*} Password length should be of 8 with combination of at least one alphabet, digit & one special character (ex:fia@1234)');
                document.getElementById('txtNewPwd').value = "";
                document.getElementById('txtNewPwd').focus();
                document.getElementById('txtConfirmPwd').value = "";
                return false;
            }
        }
    </script>
    <style type="text/css">
        .mymarquee1
{
    margin: 1px auto 0px auto;
    padding: 1px;
    border: 1px solid #e5e5e5;
}
       .mymarquee2
{
    margin: 1px auto 0px auto;
    padding: 1px;
    border: 1px solid #e5e5e5;
    color :blue ;
}
    </style>
	 
    <script  type="text/javascript">
        var Processorid = ""
        var computername = ""
        var MbSerial = ""
        "use strict";
        var strComputer = '.';
        var SWBemlocator = new ActiveXObject("WbemScripting.SWbemLocator");
        var wmi = SWBemlocator.ConnectServer(strComputer, "/root/CIMV2");
        //var properties = wmi. ExecQuery ("SELECT * from win32_diskdrive");
        var properties = wmi.ExecQuery("SELECT * from Win32_Processor");
        var e = new Enumerator(properties);
        var p = e.item();

        var properties1 = wmi.ExecQuery("SELECT * from Win32_baseboard");
        var e1 = new Enumerator(properties1);
        var p1 = e1.item();
        Processorid = p.processorid
        computername = p.systemname
        MbSerial = p1.serialnumber
        

  </script>
    <script type="text/javascript">
        function ShowButtton() {
            var seconds = 29;
            var timeleft = 29;
            var downloadTimer = setInterval(function () {
                if (timeleft <= 0) {
                    clearInterval(downloadTimer);
                    document.getElementById("countdown").style.display = "none";
                } else {
                    document.getElementById("countdown").innerHTML = timeleft + " seconds remaining";
                }
                timeleft -= 1;
            }, 1000);
            setTimeout(function () {
             
              document.getElementById("resend").style.display = "block";
          }, seconds * 1000);
        };
    </script>
   
</head>
<body>
    <form method="post" action="./Login.aspx?ReturnUrl=%2f" onsubmit="javascript:return WebForm_OnSubmit();" id="form1" autocomplete="off">
<div class="aspNetHidden">
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EXEKuhp9VCusD9QS4cI666M5MbjSqD8eBGvbdzW6qDumeoqbkG3b2BQVFWhB12S6qVgzpO0IJW5PTmk+w9znF+ffqB4=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZHY6OC18DCvqeZd8XfehBIlvqlDJu5na390GB7eU55SNrmJPrQ2&amp;t=637631720467544554" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=nv7asgRUU0tRmHNR2D6t1EBNgFUH6lw2yKisq2kHlweWfjNdPcMBagWtc8JQCTAK4A-X_q2jQZJgahLBXf1HU4DPsoa3ywD1XsVE172sbz3WzOBkuj9l2ocLtuHCHWEKl6BCqw2&amp;t=125d5fef" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=x6wALODbMJK5e0eRC_p1LSthuLxxU31xp04tf-u3goscvzinsG0-JEM-zFQy6IrKhmYBrBjNUFeSIKDvOQioc5YT2cS7KghkgALf4FQzXHE9v4fL0&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=P5lTttoqSeZXoYRLQMIScOVC7mhgWHikPPMMVPqQ4cXanJRmSP2mAa9BOvquuD3Tcdb-J5MaK9wYtQ97tMvyAVDduL78blVXi3mEmrtBABsXW6800&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=NHo7rzvB81m25b5lv-ojMb1lYRI-99mqWAd7Loc9n2FvS_AKIbhgC_div35BS_G6iNRQ1x33Sh7FGuiisHHa1LYa0x4hYnUQwMPQuD91eOhlZYOE0&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=cwGphBcvejt2VIyBHnRhTbZApsGbUtupoTAnJSNTrZ54IVvuAMCS6rZ4SgM2nGu1T6vRZqpEEvjB_e0YNHe5DnqEdGJyTO2dXx0fYPYUpsleKrbj0&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ZT9rkJNR62Xa35oVFu0bre-skacXYR7-f23JcrfcbiDWfKdZX5kqMbZFe_xxuDq6xd4WVCc6MhLJhc2GJWFzxuFSqVhjOJMgEM1TCxvGyCoNwu4sPxM0LNLSjDOJMXAyZel_ig2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=G7OZOzpYF9XwHB8eN29Yb-WeNjraQKodJX-rqT2hCCFiUQPWE4Ys1HJoEYZPzgsnFFEAMvLEDpskY0vgtsnh98lAxQhgb3zIAVHDoFcSm3gdvT2sKzgUN7GlYjZR4pAfbZev-w2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=dyNR31g23bihdQsCIkXUOK5rQC2I1lMX7A5uOaHVaHeJ01BsfSqGsZs0vgixmF-tlwOtd4gXobW9PsuZP270L6evEqHnshaXgldORVKLkqaSFTcuvFlC8HDS5OilQGto4lGIBQ2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=t8xKct_NbNgBw-XPoRZ-nBG4g4EXtx7j2F_9H0KRBuDeqyolFl91wNE1ndC9KTHdHqH9h2CMKJwKb8hyB4FSh8I5S8_yRqnKK2mPofz8Rcbs4GdPW96WLczNlFMvO4UM9F7ykg2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TLZ1vtjBxtO9Jbc2uVK03-CMPh0dlr61urA7V62457pOs-1Yqh9j93Ci72xlGK_A8iN-feIyExs62jSSu1z75Ahep7itbnfwngRXgmZIpZ7RfxU0MRZhFUIVGsCgogCgzt6dDg2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=IDWj4WSOOvkdPRnPVbJVZO6DQyp5cAp3O8MRtCwoGmrjMYew8WORITjnh4L1fTXGeEkOLiAKduKSo8msYRGJMaD4lrqCqm-oJ4XOXUilXzgnY0Kq0&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=QURf4xOsrV7WTJCWSccZWwqRapsJkVAfh3THJFmAH6nsyBlyRVdLvDri6D470nzUVMBV_z23vV75uOwVZOio3hs2MP99wuiH3WCSqbJIGKVX1pn33pGAF6dCDyl3F8pElBgeyw2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=APiGnkSUwteJqB7qVaRTTnohHs9aUXk-aqjy0MqcZJU1bl4Olj4562sOsncxooLiBPPjJ_4rZ4cFFQF3psz5yyHxqi4Ug35wciLwkaHm-4MS06Q9n-cgtBLGJD-5oFr9qomR5g2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pu2ExBiwxefn6RsV2td6qxU0bX0koORQI2VwVtKiB0lxgYNbXGfkPuDY7tScaIt7fTHKJ83jDhMtOYGLU86qU439qSh0JzUtAuNUD7AfqTSlIxPVSuR8xR6lfHdmM1bot4K-Dw2&amp;t=7f16157c" type="text/javascript"></script>
<script src="/WebResource.axd?d=JoBkLzP19aTuxbWOhHobYsE8X2AZqJDMlRDE7_n1TzXQVSCz3CgwyBHsCoOLwzJJCzCHXw2&amp;t=637631720467544554" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C2EE9ABB" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="t+X/QIYM7ht5MNg/loMBPF0vWG0i2aTBXOsfSl7nv6WXNSdGzSqBZ9VBaPLqeprcMO4T1IoiYwH3Pm6RiCh8/Tn0vjWMoNBWf8HtwXsh+gNxLrsbl/0Pu7inT+aRPVi8YBcLX/xwEroeZ4kkmIssG+mbmeGebcmIYIduQLFd3j6nGuphpv/MxUL2EGsywWDOoalL32/S1lbbK9qRisCobHVPNnJFq7kgP57G+QhxuJIGl+1NZOU4X5hwS3KYZOFRwo06UlO6DMa2bXRzB0nniig0zjGj4bXeDFV0bQWtMR9qMg86" />
</div>
    <div class="header">
        <table cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr>
                <td>
                    <img src="Images/Brand/fialogo.png" alt="" />
                </td>
                <td align="right" valign="top">
                    <img src="Images/Brand/applogo.png" width="250px" alt="" />
                </td>
            </tr>
        </table>
    </div>
    <div class="main">
        <table border="0" cellpadding="10" cellspacing="0" style="background: #fff; padding: 0;
            margin: 0 auto; width: 86%;">
            <tr>
                <td valign="top">
                    <div class="mail">
                        <div class="TabbedPanels" id="tp1">
                            <ul class="TabbedPanelsTabGroup">
                                  
                                <li class="TabbedPanelsTab TabbedPanelsTabSelected" tabindex="0">About Us</li>
                                <li class="TabbedPanelsTab thirdTab" tabindex="0">Services</li>
                                <li class="TabbedPanelsTab lastTab" tabindex="0">Contact Us</li>
                            </ul>
                            <div class="TabbedPanelsContentGroup">
                                 

                                <div style="display: none;" class="TabbedPanelsContent">
                                    <div class="content">
                                         <p>FIA plans to elevate Asia's unserved geographies through its core vision of ‘economic and social inclusion’. We promote sustainable livelihood models and work with marginal communities, rural poor and women. FIA aggregates socially impactful products and services and delivers them to low income masses through our extensive distribution network of micro-entrepreneurs. FIA’s expertise lies in developing technology for mobility and online payment solutions for the masses, creating and managing distribution networks for delivering services to the last mile and implementing financial inclusion programs. </p>
                                       <p>SERVING THE UNSERVED OF ASIA The stark reality is that most poor people in the world still lack access to sustainable financial services, whether it is savings, credit or insurance. In India, this translates to 400 million people who manage their finances on a cash-only basis and have restricted or no access to financial services.</p>
                                       <p>The greatest challenge is to relax the constraints that exclude people from full participation in the financial sector. This is where FIA comes in. A company started by a team of students from MIT, FIA makes the economics work for banks to serve the underserved. FIA aims at popularizing the use of alternate technologies for enhancing the efficiency and security of financial transactions. Our award-winning model combines an extensive distribution channel and product portfolio to bridge the huge demand-supply gap for financial products and services in under-served geographies. </p>
                                        <p>FIA’s aim is to bring about an evolution in how the under-served can capitalize on the digital growth opportunity. Our goal is to create the largest network of FIA Inclusion Centers to enable individuals to avail an array of services – Financial inclusion, Skill Development, Livelihood Opportunities, Digital Payment Solutions and access other socially impactful products and services.</p>
                                    </div>
                                </div>
                                <div style="display: none;" class="TabbedPanelsContent">
                                    <div class="content">
                                        Customers can pay utility bills, buy insurance, remit money, recharge mobile bills
                                        or DTH TV, Book rail and air tickets etc.
                                        <br />
                                        Some of the services that we offer are:
                                        <ul id="conetnt1">
                                            <li class="listitem1">Banking</li>
                                            <li class="listitem1">Remittance</li>
                                            <li class="listitem1">Prepaid Recharge</li>
                                            <li class="listitem1">Postpaid Mobile Payments</li>
                                            <li class="listitem1">Utility Bill Payments</li>
                                            <li class="listitem1">Insurance</li>
                                            <li class="listitem1">Tickets</li>
                                             <li class="listitem1">Pancard Services</li>
<li class="listitem1">Nepal Money Transfer</li>
                                        </ul>
                                    </div>
                                </div>
                                 <div style="display: none;" class="TabbedPanelsContent">
                                    <div class="content">
                                        <b>FIA Technology Services Pvt. Ltd.</b>
                                        <br />
                                      Unit no. 504-507, 5th Floor JMD Megapolis Sohna Road
                                        <br />
                                        Sector 48, Gurgaon
                                        <br />
                                        Haryana, INDIA 122018
                                        <br />
                                        Tel: 0124-4993100, 01, 02
                                        <br />
                                        Fax: +91-0124-4993102
                                        <br />
                                        Email: <a href="mailto:ccare@fiaglobal.com">ccare@fiaglobal.com</a>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td valign="middle">
                    <div class="mail-hero-right">
                        <img src="Images/Brand/billpay.png" width="495" alt="" />
                    </div>
                </td>
                <td valign="top">
                    <div class="sign-in">
                        <div id="pnlLogin">
	
                            <div class="signin-box">
                                <h2 style="font-size: 20px; font-weight: normal; color: #f3f3f3;">
                                    Sign in</h2>
                                <hr style="border-color: #6b800b;" />
                                <div class="usercode-div" style="margin-top: 20px;">
                                    <strong class="usercode-label">UserCode</strong>
                                    <input name="txtUserCode" type="text" id="txtUserCode" autocomplete="off" onkeyup="return toUpper(this.id)" />
                                    <span id="rfvUserCode" class="star" style="display:none;">*</span>
                                </div>
                                <div class="password-div">
                                    <strong class="password-label">Password</strong>
                                    <input name="txtPassword" type="password" id="txtPassword" autocomplete="off" />
                                    <span id="rfvPassword" class="star" style="display:none;">*</span>
                                </div>
                                <input type="submit" name="btnLogin" value="Login" onclick="HashLogin();WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;btnLogin&quot;, &quot;&quot;, true, &quot;Group&quot;, &quot;&quot;, false, false))" id="btnLogin" class="g-button" />
                                <input type="submit" name="btnReset" value="Reset" id="btnReset" class="g-button" />
                                 <ul>
                           
                                    <li><a href="Anonym/PaynetzPolicy.aspx"><u>Privacy Policy</u></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ForgotPwd.aspx"><u>Forgot Password?</u></a> </li>
                                    <br />
                                     
                                </ul>
                            </div>
                        
</div>
                        
                        
                        
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
  
                <div>
               
                 <marquee class="mymarquee1"  onmouseover="stop();" onmouseout="start();">
                <h3>Published in Public Interest: This is for the information of all concerned and general public that , 'fiaglobal.com' is the only official website of FIA Technology Services Pvt. Ltd. 
FIA Technology Services Pvt. Ltd. does not have any other website to solicit application for CSP or for financial dealings with regard to appointment of agent, distributor or similar services.
FIA Technology Services Pvt. Ltd. is not responsible for information and funds shared basis links or data on fake websites that claims to have connection with the company FIA Technology Services Pvt. Ltd. 


Viewers and business partners are requested to highlight other websites, portals or links, that claims to be of FIA Technology, to the following email id: info@fiaglobal.com for immediate intervention.

 </h3>
                </marquee>
                </div>
                    <div>
                        <marquee class="mymarquee">
   <img src="Images/Partner/prabhu money.PNG" alt="" />
                           
                            <img src="Images/Partner/airtel.PNG" alt="" />
                            <img src="Images/Partner/bsnl.PNG" alt="" />
                         
                            <img src="Images/Partner/mtnl.PNG" alt="" />
                            <img src="Images/Partner/mts.PNG" alt="" />
                   
                            
                        </marquee>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="footer">
        FIA Technology Services Pvt. Ltd. All rights Reserved | Call Center: +91- 0124-4796530 &amp; Email: ccare@fiaglobal.com | <a href="Anonym/DocViewer.aspx" style="color: #ffffff; text-decoration: underline;" target="_blank"> Terms & Conditions</a><br />
        This Website is best viewed in Internet Explorer 8+ with resolution 1024 X 768
    </div>
 <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'form1', [], [], [], 90, '');
//]]>
</script>

    <div id="pnlPushIn" class="hcenter">
	
                    <table>
                        <colgroup>
                            <col width="50%" />
                            <col width="*" />
                        </colgroup>
                       
                        <tr>
                            <td align="right">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                       
                        <tr>
                            <td align="left" colspan="2">
                                <span id="lblmsg">Label</span>
                            </td>
                        </tr>
                       
                        <tr>
                            <td align="right">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <input type="submit" name="btnPushIn" value="Yes" onclick="return confirmation();" id="btnPushIn" class="g-button" />
                                &nbsp;<input type="submit" name="btnCancel" value="No" id="btnCancel" class="g-button" />
                            </td>
                        </tr>
                    </table>
                    <a id="lnkFake" href="javascript:__doPostBack(&#39;lnkFake&#39;,&#39;&#39;)"></a>
                    
                
</div>
				 <div style="display:none;" >
                   <input name="txtmac1" type="text" id="txtmac1" />
    <input name="txtmac2" type="text" id="txtmac2" />
	<input name="txtmac3" type="text" id="txtmac3" /> 
                </div>
    
<script type="text/javascript">
//<![CDATA[
var Page_Validators =  new Array(document.getElementById("rfvUserCode"), document.getElementById("rfvPassword"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var rfvUserCode = document.all ? document.all["rfvUserCode"] : document.getElementById("rfvUserCode");
rfvUserCode.controltovalidate = "txtUserCode";
rfvUserCode.focusOnError = "t";
rfvUserCode.display = "Dynamic";
rfvUserCode.validationGroup = "Group";
rfvUserCode.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
rfvUserCode.initialvalue = "";
var rfvPassword = document.all ? document.all["rfvPassword"] : document.getElementById("rfvPassword");
rfvPassword.controltovalidate = "txtPassword";
rfvPassword.focusOnError = "t";
rfvPassword.display = "Dynamic";
rfvPassword.validationGroup = "Group";
rfvPassword.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
rfvPassword.initialvalue = "";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        WebForm_AutoFocus('txtUserCode');
document.getElementById('rfvUserCode').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('rfvUserCode'));
}

document.getElementById('rfvPassword').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('rfvPassword'));
}
Sys.Application.add_init(function() {
    $create(Sys.Extended.UI.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","CancelControlID":"btnCancel","PopupControlID":"pnlPushIn","dynamicServicePath":"/Login.aspx","id":"ModalPopupExtender1","repositionMode":1}, null, null, $get("lnkFake"));
});
//]]>
</script>
</form>
    <script type="text/javascript">

        var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
				
    </script>
    <script language="JavaScript" type="text/javascript">
        var tp1 = new Spry.Widget.TabbedPanels("tp1", { interval: 3000 });
        tp1.start();
    </script>
	<script type="text/javascript">

         document.getElementById("txtmac1").value = unescape(Processorid);
         document.getElementById("txtmac2").value = unescape(computername);
         document.getElementById("txtmac3").value = unescape(MbSerial);
    
</script>
</body>
</html>
