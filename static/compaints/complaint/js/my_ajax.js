var xmlHttp
var xmlHttp

function showDv(str) {
    // alert(str);
    if (typeof XMLHttpRequest != "undefined") {
        xmlHttp = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (xmlHttp == null) {
        alert("Browser does not support XMLHTTP Request")
        return;
    }
    var url = "dv_select.jsp";
    url += "?stname=" + str;
    xmlHttp.onreadystatechange = dvChange;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function dvChange() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("division").innerHTML = xmlHttp.responseText
    }
}

function showDist(str) {
    if (typeof XMLHttpRequest != "undefined") {
        xmlHttp = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (xmlHttp == null) {
        alert("Browser does not support XMLHTTP Request")
        return;
    }
    var url = "dist_select.jsp";
    url += "?division=" + str;
    xmlHttp.onreadystatechange = distChange1;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function distChange1() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("dist").innerHTML = xmlHttp.responseText
    }
}