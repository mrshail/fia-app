<!DOCTYPE html>




<html lang="en">
<head>
  <title>Guest Complain Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="images/title_icon.png"/>
  <script src="js/jquery.validate.min.js"></script>
  <script src="js/form-validation.js"></script>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  <script type="text/javascript" src="js/my_ajax.js"></script>
  <script src="bootstrap_files/jquery.min.js"></script>
  <script src="bootstrap_files/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="bootstrap_files/bootstrap.min.css">
 
  <style type="text/css">
  body{
  font-family: Times New Roman;
  color: #000000;
  }

#astric{
color: red;
}
#legend {
    background: #1f497d none repeat scroll 0 0;
    border-radius: 3px;
    box-shadow: 0 0 0 3px #ddd;
    color: #fff;
    font-size: 20px;
    margin-top: -20px;
    padding: 5px 10px;
    width: 27%
}
  </style>
</head>
<body>
<div class="container" style="padding-bottom:20px; padding-top: 10px;">
  <div class="row">
    <div class="col-sm-12" style="border:2px solid; border-radius:10px 10px 0px 0px; text-align: center;text-transform:capitalize;">
    <br>
    <img src="images/nict_logo.png" height="100" width="100" />
      <h2><strong>NICT Technologies Pvt. Ltd.</strong></h2>
      <br>
    </div>
  <form action="save_guest_complain.jsp" method="post" name="myform">  
 <div class="col-sm-12" style="border:2px solid; text-align: center; ">
    <p id="legend">Guest Details</p>
 <div class="col-sm-12">
  <div class="form-group">
    <div class="col-sm-4">
         <label for="state">State<span id="astric">*</span></label>
                <select style="color: black ;" name="state" id="state" onchange="showDv(this.value);" required="required" class="form-control input-normal">
                    <option selected="selected" disabled="disabled" value="">Select</option>
		  
      	<option value="ANDHRA PRADESH">ANDHRA PRADESH</option>  
      	
      	<option value="ARUNACHAL PRADESH">ARUNACHAL PRADESH</option>  
      	
      	<option value="ASSAM">ASSAM</option>  
      	
      	<option value="BIHAR">BIHAR</option>  
      	
      	<option value="CHHATTISGARH">CHHATTISGARH</option>  
      	
      	<option value="GUJARAT">GUJARAT</option>  
      	
      	<option value="HARYANA">HARYANA</option>  
      	
      	<option value="HIMACHAL PRADESH">HIMACHAL PRADESH</option>  
      	
      	<option value="JAMMU & KASHMIR">JAMMU & KASHMIR</option>  
      	
      	<option value="JHARKHAND">JHARKHAND</option>  
      	
      	<option value="KARNATAKA">KARNATAKA</option>  
      	
      	<option value="MADHYA PRADESH">MADHYA PRADESH</option>  
      	
      	<option value="MAHARASHTRA">MAHARASHTRA</option>  
      	
      	<option value="MANIPUR">MANIPUR</option>  
      	
      	<option value="MEGHALAYA">MEGHALAYA</option>  
      	
      	<option value="MIZORAM">MIZORAM</option>  
      	
      	<option value="NAGALAND">NAGALAND</option>  
      	
      	<option value="NCT OF DELHI">NCT OF DELHI</option>  
      	
      	<option value="ODISHA">ODISHA</option>  
      	
      	<option value="PUNJAB">PUNJAB</option>  
      	
      	<option value="RAJASTHAN">RAJASTHAN</option>  
      	
      	<option value="SIKKIM">SIKKIM</option>  
      	
      	<option value="TRIPURA">TRIPURA</option>  
      	
      	<option value="UTTAR PRADESH">UTTAR PRADESH</option>  
      	
      	<option value="UTTARAKHAND">UTTARAKHAND</option>  
      	
      	<option value="WEST BENGAL">WEST BENGAL</option>  
      	
		</select>
		</div>
		
		
		 <div class="col-sm-4">
         <label for="division">Division<span id="astric">*</span></label>
                <select style="color: black ;" name="division" id="division" onchange="showDist(this.value);" required="required" class="form-control input-normal">
      			<option disabled="disabled" value="">First Select State</option>  
		</select></div>
		
		
		 <div class="col-sm-4">
         <label for="dist">District<span id="astric">*</span></label>
                <select style="color: black ;" name="dist" id="dist" required="required" class="form-control input-normal">
      			<option disabled="disabled" value="">First Select Division</option>  
				</select>
		</div>
		</div></div>
		<p>&#160</p>
	<div class="col-sm-12">
  	<div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name <span id="astric">*</span></label>
        <div class="col-sm-4"><input name="name" id="name" onselect="return formSubmit()" class="form-control" type="text">
       		 <span id="name_error" style="color: red"></span>
     	</div>
      
      	<label for="mobile" class="col-sm-2 control-label">Mobile <span id="astric">*</span></label>
         <div class="col-sm-4"><input name="mobile" onselect="return formSubmit()" id="mobile" maxlength="10" class="form-control" type="text">
         	 <span id="mobile_error" style="color: red"></span>
      	 </div>
      </div></div>
      <p>&#160</p>
      
      
    <div class="col-sm-12">
  	<div class="form-group">
       <label for="serviceName" class="col-sm-2 control-label">Service Name : <span id="astric"></span></label>
       <div class="col-sm-4">
		<!--
		<input name="serviceName" id="serviceName" class="form-control" type="text">
		-->
		<select name="serviceName" id="serviceName" class="form-control">
			<option value="Banking">Banking</option>
			<option value="MPPKVVCL">MPPKVVCL</option>
			<option value="FasTag RFID">FasTag RFID</option>
			<option value="UID">UID</option>
			<option value="Pancard">Pancard</option>
			<option value="eMitra">eMitra</option>
			<option value="IRCTC">IRCTC</option>
			<option value="Others">Others</option>
		</select>
	   
<!--            <span id="bank_error" style="color: red"></span> -->
      </div>
      
      <label for="serviceId" class="col-sm-2 control-label">Service Id / Kiosk Id / Bank Id : <span id="astric"></span></label>
      <div class="col-sm-4"><input name="serviceId" id="serviceId" maxlength="20" class="form-control" type="text">
<!--       	 <span id="serv_id_error" style="color: red"></span>	 -->
          <br><br>
     </div>
    </div>
	</div>		
    </div>
    		<br><br>
     <div class="col-sm-12" style="border:2px solid; border-radius:0px 0px 10px 10px; padding-bottom:10px ; text-align: center;text-transform:capitalize;">
     <p id="legend">Complaint Box</p>
     <h4>If You Have Any Questions, Comments, Complaint Or Requests, Please Feel Free To Contact Us.</h4>
		 <div class="form-group">
  			<label for="Message">Your Message<span id="astric">*</span></label>
  			<textarea class="form-control" rows="4" name="msg" required="required"  id="msg" maxlength="320"></textarea>
		</div>
		<button type="reset" class="btn btn-default"><strong>Clear</strong></button>
		<button type="submit" onClick="return formSubmit()" class="btn btn-primary"><strong>Submit</strong></button>
		
		</div>
	</form>
    </div>
  </div>
  <br><br>

</body>
</html>
