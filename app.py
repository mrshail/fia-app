from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import random as rand
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////complaints.db'
app.config['SQLACHEMY_TRACK_MODIFICATIONS']=False
db = SQLAlchemy(app)
app.app_context().push()

#class complaints
class complaints(db.Model):
    
    #defined columns
    sno = db.Column(db.Integer, primary_key=True)
    state =db.Column(db.String(50), nullable =False)
    district =db.Column(db.String(50), nullable =False)
    village_town =db.Column(db.String(50), nullable =False)
    email =db.Column(db.String(50), nullable =False)
    name =db.Column(db.String(50), nullable =False)
    mobile =db.Column(db.String(10), nullable =False)
    service_name =db.Column(db.String(50), nullable =False)
    service_id =db.Column(db.String(50), nullable =False)
    message =db.Column(db.String(250), nullable =False)
    #ticket_number=db.Column(db.String, nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)



    def __repr__(self) -> str:
        return f"{self.sno} - {self.name} - {self.mobile} - {self.service_name} - {self.service_id} - {self.message}"
    

@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template("home.html")

@app.route('/our_network',  methods=['GET', 'POST'])
def our_network():
    return render_template("our_network.html")


@app.route('/banks' ,methods=['GET', 'POST'])
def banks():
    return render_template("banks.html")

@app.route('/partners' ,methods=['GET', 'POST'])
def partners():
    return render_template("partners.html")

@app.route('/complaints' ,methods=['GET', 'POST'])
def all_complaints():
    return render_template("complaints.html")

@app.route('/guest' ,methods=['GET', 'POST'])
def guest():
    return render_template("guest.html")

@app.route('/thankyou' ,methods=['GET', 'POST'])
def thankyou():
    if request.method=='POST':
        
        state=request.form['state']
        district=request.form['district']
        village_town=request.form['village_town']
        email=request.form['email']
        name=request.form['name']
        service_name=request.form['service_name']
        service_id=request.form['service_id']
        mobile=request.form['mobile']
        message=request.form['message']
        #ticket_number= request.form['ticket_number']
        complaint=complaints(state = state, district=district, village_town=village_town, email=email,  name=name, mobile=mobile, service_name=service_name, service_id=service_id, message=message)#, ticket_number=ticket_number)
        db.session.add(complaint)
        db.session.commit()

    allComplaints=complaints.query.all()
    return render_template("thankyou.html", allComplaints=allComplaints)

@app.route('/received_complaints' ,methods=['GET', 'POST'])
def received_complaints():
    if request.method=='POST':
        
        state=request.form['state']
        district=request.form['district']
        village_town=request.form['village_town']
        email=request.form['email']
        name=request.form['name']
        service_name=request.form['service_name']
        service_id=request.form['service_id']
        mobile=request.form['mobile']
        message=request.form['message']

        complaint=complaints(state = state, district=district, village_town=village_town, email=email,  name=name, mobile=mobile, service_name=service_name, service_id=service_id, message=message)
        db.session.add(complaint)
        db.session.commit()

    allComplaints=complaints.query.all()
    return render_template("received_comp.html", allComplaints=allComplaints)

@app.route('/existing_csp' ,methods=['GET', 'POST'])
def existing_csp():
    return render_template("existing_csp.html")


@app.route('/bill_pay' ,methods=['GET', 'POST'])
def bill_pay():
    return render_template("bill_pay.html")



if __name__=="__main__":
    app.run(debug=True, port = 5000)